﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FoodtruckData;

namespace Foodtruck.Controllers
{
    public class ActualitésController : Controller
    {
        private FoodTruckEntities db = new FoodTruckEntities();

        // GET: Actualités
        public ActionResult Index()
        {
            return View(db.Actualités.ToList());
        }

        // GET: Actualités/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Actualités actualités = db.Actualités.Find(id);
            if (actualités == null)
            {
                return HttpNotFound();
            }
            return View(actualités);
        }

        // GET: Actualités/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Actualités/Create
        // Pour vous protéger des attaques par survalidation, activez les propriétés spécifiques auxquelles vous souhaitez vous lier. Pour 
        // plus de détails, consultez https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id_actualite,titre,description,image")] Actualités actualités)
        {
            if (ModelState.IsValid)
            {
                db.Actualités.Add(actualités);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(actualités);
        }

        // GET: Actualités/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Actualités actualités = db.Actualités.Find(id);
            if (actualités == null)
            {
                return HttpNotFound();
            }
            return View(actualités);
        }

        // POST: Actualités/Edit/5
        // Pour vous protéger des attaques par survalidation, activez les propriétés spécifiques auxquelles vous souhaitez vous lier. Pour 
        // plus de détails, consultez https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id_actualite,titre,description,image")] Actualités actualités)
        {
            if (ModelState.IsValid)
            {
                db.Entry(actualités).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(actualités);
        }

        // GET: Actualités/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Actualités actualités = db.Actualités.Find(id);
            if (actualités == null)
            {
                return HttpNotFound();
            }
            return View(actualités);
        }

        // POST: Actualités/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Actualités actualités = db.Actualités.Find(id);
            db.Actualités.Remove(actualités);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
