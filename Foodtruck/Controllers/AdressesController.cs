﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FoodtruckData;
using Microsoft.AspNet.Identity;

namespace Foodtruck.Controllers
{
    public class AdressesController : Controller
    {
        private FoodTruckEntities db = new FoodTruckEntities();

        // GET: Adresses
        public ActionResult Index()
        {
            var adresse = db.Adresse.Include(a => a.Adresse_type).Include(a => a.Utilisateur);
            return View(adresse.ToList());
        }

        // GET: Adresses/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Adresse adresse = db.Adresse.Find(id);
            if (adresse == null)
            {
                return HttpNotFound();
            }
            return View(adresse);
        }

        // GET: Adresses/Create
        public ActionResult Create()
        {
            
            ViewBag.id_adresse = new SelectList(db.Adresse_type, "id_adresse_type", "libelle");
            ViewBag.id_adresse = new SelectList(db.Utilisateur, "id_utilisateur", "nom");
            return View();
        }

        // POST: Adresses/Create
        // Pour vous protéger des attaques par survalidation, activez les propriétés spécifiques auxquelles vous souhaitez vous lier. Pour 
        // plus de détails, consultez https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Adresse adresse)
        {
            if (ModelState.IsValid)
            {
                adresse.id_adresse = db.Adresse.Max(f => f.id_adresse) + 1;
                var email = db.AspNetUsers.Find(User.Identity.GetUserId()).Email;
                var user = db.Utilisateur.FirstOrDefault(f => f.email.Equals(email));
                adresse.id_utilisateur = user.id_utilisateur;
                db.Adresse.Add(adresse);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.id_adresse = new SelectList(db.Adresse_type, "id_adresse_type", "libelle", adresse.id_adresse);
            ViewBag.id_adresse = new SelectList(db.Utilisateur, "id_utilisateur", "nom", adresse.id_adresse);
            return View(adresse);
        }

        // GET: Adresses/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Adresse adresse = db.Adresse.Find(id);
            if (adresse == null)
            {
                return HttpNotFound();
            }
            ViewBag.id_adresse = new SelectList(db.Adresse_type, "id_adresse_type", "libelle", adresse.id_adresse);
            ViewBag.id_adresse = new SelectList(db.Utilisateur, "id_utilisateur", "nom", adresse.id_adresse);
            return View(adresse);
        }

        // POST: Adresses/Edit/5
        // Pour vous protéger des attaques par survalidation, activez les propriétés spécifiques auxquelles vous souhaitez vous lier. Pour 
        // plus de détails, consultez https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id_adresse,numero_de_rue,rue,ville,pays,id_utilisateur,id_adresse_type,activite,code_postal")] Adresse adresse)
        {
            if (ModelState.IsValid)
            {
                db.Entry(adresse).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.id_adresse = new SelectList(db.Adresse_type, "id_adresse_type", "libelle", adresse.id_adresse);
            ViewBag.id_adresse = new SelectList(db.Utilisateur, "id_utilisateur", "nom", adresse.id_adresse);
            return View(adresse);
        }

        // GET: Adresses/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Adresse adresse = db.Adresse.Find(id);
            if (adresse == null)
            {
                return HttpNotFound();
            }
            return View(adresse);
        }

        // POST: Adresses/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Adresse adresse = db.Adresse.Find(id);
            db.Adresse.Remove(adresse);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
