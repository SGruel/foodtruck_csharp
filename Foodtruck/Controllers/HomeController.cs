﻿using Foodtruck.Models;
using FoodtruckData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Foodtruck.Controllers
{
    public class HomeController : Controller
    {

        private FoodTruckEntities db = new FoodTruckEntities();

        public ActionResult Index()
        {
            var listActu = db.Actualités.ToList();

            var prodSucces = db.Produits.OrderBy(f => f.stock>0).Take(3).ToList();

            ActualitesTopProd dto = new ActualitesTopProd
            {
                Actus = listActu,
                Prod = prodSucces
            };


            return View(dto);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}