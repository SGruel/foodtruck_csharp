﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FoodtruckData;

namespace Foodtruck.Controllers
{
    public class ProduitsController : Controller
    {
        private FoodTruckEntities db = new FoodTruckEntities();

        // GET: Produits
        public ActionResult Index()
        {
            var produits = db.Produits.Include(p => p.Famille_repas).Include(p => p.Ligne_commande);
            return View(produits.ToList());
        }

        // GET: Produits/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Produits produits = db.Produits.Find(id);
            if (produits == null)
            {
                return HttpNotFound();
            }
            return View(produits);
        }

        // GET: Produits/Create
        public ActionResult Create()
        {
            ViewBag.id_produit = new SelectList(db.Famille_repas, "id_famille_repas", "libelle");
            ViewBag.id_produit = new SelectList(db.Ligne_commande, "id_ligne_commande", "id_ligne_commande");
            return View();
        }

        // POST: Produits/Create
        // Pour vous protéger des attaques par survalidation, activez les propriétés spécifiques auxquelles vous souhaitez vous lier. Pour 
        // plus de détails, consultez https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id_produit,libelle_commercial,libelle_technique,description,stock,prix_unitaire,id_famille_repas")] Produits produits)
        {
            if (ModelState.IsValid)
            {
                db.Produits.Add(produits);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.id_produit = new SelectList(db.Famille_repas, "id_famille_repas", "libelle", produits.id_produit);
            ViewBag.id_produit = new SelectList(db.Ligne_commande, "id_ligne_commande", "id_ligne_commande", produits.id_produit);
            return View(produits);
        }

        // GET: Produits/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Produits produits = db.Produits.Find(id);
            if (produits == null)
            {
                return HttpNotFound();
            }
            ViewBag.id_produit = new SelectList(db.Famille_repas, "id_famille_repas", "libelle", produits.id_produit);
            ViewBag.id_produit = new SelectList(db.Ligne_commande, "id_ligne_commande", "id_ligne_commande", produits.id_produit);
            return View(produits);
        }

        // POST: Produits/Edit/5
        // Pour vous protéger des attaques par survalidation, activez les propriétés spécifiques auxquelles vous souhaitez vous lier. Pour 
        // plus de détails, consultez https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id_produit,libelle_commercial,libelle_technique,description,stock,prix_unitaire,id_famille_repas")] Produits produits)
        {
            if (ModelState.IsValid)
            {
                db.Entry(produits).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.id_produit = new SelectList(db.Famille_repas, "id_famille_repas", "libelle", produits.id_produit);
            ViewBag.id_produit = new SelectList(db.Ligne_commande, "id_ligne_commande", "id_ligne_commande", produits.id_produit);
            return View(produits);
        }

        // GET: Produits/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Produits produits = db.Produits.Find(id);
            if (produits == null)
            {
                return HttpNotFound();
            }
            return View(produits);
        }

        // POST: Produits/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Produits produits = db.Produits.Find(id);
            db.Produits.Remove(produits);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
