﻿using FoodtruckData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Foodtruck.Models
{
    public class ActualitesTopProd
    {
        public List<Actualités> Actus { get; set; }
        public List<Produits> Prod { get; set; }
    }
}