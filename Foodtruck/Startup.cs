﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Foodtruck.Startup))]
namespace Foodtruck
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
