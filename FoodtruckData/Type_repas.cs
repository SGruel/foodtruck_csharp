//------------------------------------------------------------------------------
// <auto-generated>
//     Ce code a été généré à partir d'un modèle.
//
//     Des modifications manuelles apportées à ce fichier peuvent conduire à un comportement inattendu de votre application.
//     Les modifications manuelles apportées à ce fichier sont remplacées si le code est régénéré.
// </auto-generated>
//------------------------------------------------------------------------------

namespace FoodtruckData
{
    using System;
    using System.Collections.Generic;
    
    public partial class Type_repas
    {
        public int id_repas { get; set; }
        public Nullable<int> id_famille_repas { get; set; }
    
        public virtual Famille_repas Famille_repas { get; set; }
        public virtual Repas Repas { get; set; }
    }
}
