USE [master]
GO
/****** Object:  Database [FoodTruck]    Script Date: 10/07/2020 11:13:05 ******/
CREATE DATABASE [FoodTruck]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'FoodTruck', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.SQLEXPRESS\MSSQL\DATA\FoodTruck.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'FoodTruck_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.SQLEXPRESS\MSSQL\DATA\FoodTruck_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [FoodTruck] SET COMPATIBILITY_LEVEL = 140
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [FoodTruck].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [FoodTruck] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [FoodTruck] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [FoodTruck] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [FoodTruck] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [FoodTruck] SET ARITHABORT OFF 
GO
ALTER DATABASE [FoodTruck] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [FoodTruck] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [FoodTruck] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [FoodTruck] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [FoodTruck] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [FoodTruck] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [FoodTruck] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [FoodTruck] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [FoodTruck] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [FoodTruck] SET  DISABLE_BROKER 
GO
ALTER DATABASE [FoodTruck] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [FoodTruck] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [FoodTruck] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [FoodTruck] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [FoodTruck] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [FoodTruck] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [FoodTruck] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [FoodTruck] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [FoodTruck] SET  MULTI_USER 
GO
ALTER DATABASE [FoodTruck] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [FoodTruck] SET DB_CHAINING OFF 
GO
ALTER DATABASE [FoodTruck] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [FoodTruck] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [FoodTruck] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [FoodTruck] SET QUERY_STORE = OFF
GO
USE [FoodTruck]
GO
/****** Object:  Table [dbo].[Actualités]    Script Date: 10/07/2020 11:13:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Actualités](
	[id_actualite] [int] NOT NULL,
	[titre] [varchar](50) NULL,
	[description] [text] NULL,
	[image] [varchar](250) NULL,
 CONSTRAINT [PK_Actualités] PRIMARY KEY CLUSTERED 
(
	[id_actualite] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Adresse]    Script Date: 10/07/2020 11:13:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Adresse](
	[id_adresse] [int] NOT NULL,
	[numero_de_rue] [int] NULL,
	[rue] [varchar](50) NULL,
	[ville] [int] NULL,
	[pays] [varchar](20) NULL,
	[id_utilisateur] [int] NULL,
	[id_adresse_type] [int] NULL,
	[activite] [int] NULL,
	[code_postal] [int] NULL,
 CONSTRAINT [PK_Adresse] PRIMARY KEY CLUSTERED 
(
	[id_adresse] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Adresse_type]    Script Date: 10/07/2020 11:13:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Adresse_type](
	[id_adresse_type] [int] NOT NULL,
	[libelle] [varchar](10) NULL,
 CONSTRAINT [PK_Adresse_type] PRIMARY KEY CLUSTERED 
(
	[id_adresse_type] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Commandes]    Script Date: 10/07/2020 11:13:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Commandes](
	[id_commande] [int] NOT NULL,
	[prix_total] [int] NULL,
	[date_commande] [date] NULL,
	[numero_facture] [varchar](10) NULL,
	[id_utilisateur] [int] NULL,
	[id_statut_commande] [int] NULL,
	[id_adresse_type] [int] NULL,
 CONSTRAINT [PK_Commandes] PRIMARY KEY CLUSTERED 
(
	[id_commande] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Commentaires]    Script Date: 10/07/2020 11:13:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Commentaires](
	[id_commentaire] [int] NOT NULL,
	[commentaire] [text] NULL,
	[note] [int] NULL,
	[date_commentaire] [date] NULL,
	[id_ligne_commande] [int] NULL,
 CONSTRAINT [PK_Commentaires] PRIMARY KEY CLUSTERED 
(
	[id_commentaire] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Famille_repas]    Script Date: 10/07/2020 11:13:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Famille_repas](
	[id_famille_repas] [int] NOT NULL,
	[libelle] [varchar](10) NULL,
	[activite] [int] NULL,
 CONSTRAINT [PK_Famille_repas] PRIMARY KEY CLUSTERED 
(
	[id_famille_repas] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Genre]    Script Date: 10/07/2020 11:13:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Genre](
	[id_genre] [int] NOT NULL,
	[libelle] [varchar](5) NULL,
 CONSTRAINT [PK_Genre] PRIMARY KEY CLUSTERED 
(
	[id_genre] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Ligne_commande]    Script Date: 10/07/2020 11:13:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Ligne_commande](
	[id_ligne_commande] [int] NOT NULL,
	[quantite] [int] NULL,
	[prix_unitaire] [int] NULL,
	[id_commande] [int] NULL,
	[id_produit] [int] NULL,
 CONSTRAINT [PK_Ligne_commande] PRIMARY KEY CLUSTERED 
(
	[id_ligne_commande] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Produits]    Script Date: 10/07/2020 11:13:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Produits](
	[id_produit] [int] NULL,
	[libelle_commercial] [varchar](20) NULL,
	[libelle_technique] [varchar](20) NULL,
	[description] [varchar](200) NULL,
	[stock] [int] NULL,
	[prix_unitaire] [int] NULL,
	[id_famille_repas] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Profil]    Script Date: 10/07/2020 11:13:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Profil](
	[id_profil] [int] NOT NULL,
	[libelle] [varchar](10) NULL,
 CONSTRAINT [PK_Profil] PRIMARY KEY CLUSTERED 
(
	[id_profil] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Repas]    Script Date: 10/07/2020 11:13:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Repas](
	[id_repas] [int] NOT NULL,
	[type_repas] [varchar](10) NULL,
	[heure_limite_commande] [time](7) NULL,
	[activite] [int] NULL,
 CONSTRAINT [PK_Repas] PRIMARY KEY CLUSTERED 
(
	[id_repas] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Statut_commande]    Script Date: 10/07/2020 11:13:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Statut_commande](
	[id_statut_commande] [int] NOT NULL,
	[libelle] [varchar](10) NULL,
 CONSTRAINT [PK_Statut_commande] PRIMARY KEY CLUSTERED 
(
	[id_statut_commande] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Type_repas]    Script Date: 10/07/2020 11:13:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Type_repas](
	[id_repas] [int] NOT NULL,
	[id_famille_repas] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Utilisateur]    Script Date: 10/07/2020 11:13:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Utilisateur](
	[id_utilisateur] [int] NOT NULL,
	[nom] [varchar](20) NULL,
	[prenom] [varchar](20) NULL,
	[date_de_naissance] [date] NULL,
	[mot_de_passe] [varchar](8) NULL,
	[login] [varchar](20) NULL,
	[email] [varchar](20) NULL,
	[id_profil] [int] NULL,
	[id_genre] [int] NULL,
	[activite] [int] NULL,
 CONSTRAINT [PK_Utilisateur] PRIMARY KEY CLUSTERED 
(
	[id_utilisateur] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Adresse]  WITH CHECK ADD  CONSTRAINT [FK_Adresse_Adresse_type] FOREIGN KEY([id_adresse])
REFERENCES [dbo].[Adresse_type] ([id_adresse_type])
GO
ALTER TABLE [dbo].[Adresse] CHECK CONSTRAINT [FK_Adresse_Adresse_type]
GO
ALTER TABLE [dbo].[Commandes]  WITH CHECK ADD  CONSTRAINT [FK_Commandes_Adresse] FOREIGN KEY([id_commande])
REFERENCES [dbo].[Adresse] ([id_adresse])
GO
ALTER TABLE [dbo].[Commandes] CHECK CONSTRAINT [FK_Commandes_Adresse]
GO
ALTER TABLE [dbo].[Commandes]  WITH CHECK ADD  CONSTRAINT [FK_Commandes_Ligne_commande] FOREIGN KEY([id_commande])
REFERENCES [dbo].[Ligne_commande] ([id_ligne_commande])
GO
ALTER TABLE [dbo].[Commandes] CHECK CONSTRAINT [FK_Commandes_Ligne_commande]
GO
ALTER TABLE [dbo].[Commandes]  WITH CHECK ADD  CONSTRAINT [FK_Commandes_Statut_commande] FOREIGN KEY([id_commande])
REFERENCES [dbo].[Statut_commande] ([id_statut_commande])
GO
ALTER TABLE [dbo].[Commandes] CHECK CONSTRAINT [FK_Commandes_Statut_commande]
GO
ALTER TABLE [dbo].[Commandes]  WITH CHECK ADD  CONSTRAINT [FK_Commandes_Utilisateur] FOREIGN KEY([id_commande])
REFERENCES [dbo].[Utilisateur] ([id_utilisateur])
GO
ALTER TABLE [dbo].[Commandes] CHECK CONSTRAINT [FK_Commandes_Utilisateur]
GO
ALTER TABLE [dbo].[Commentaires]  WITH CHECK ADD  CONSTRAINT [FK_Commentaires_Ligne_commande] FOREIGN KEY([id_ligne_commande])
REFERENCES [dbo].[Ligne_commande] ([id_ligne_commande])
GO
ALTER TABLE [dbo].[Commentaires] CHECK CONSTRAINT [FK_Commentaires_Ligne_commande]
GO
ALTER TABLE [dbo].[Type_repas]  WITH CHECK ADD  CONSTRAINT [FK_Type_repas_Famille_repas] FOREIGN KEY([id_famille_repas])
REFERENCES [dbo].[Famille_repas] ([id_famille_repas])
GO
ALTER TABLE [dbo].[Type_repas] CHECK CONSTRAINT [FK_Type_repas_Famille_repas]
GO
ALTER TABLE [dbo].[Utilisateur]  WITH CHECK ADD  CONSTRAINT [FK_Utilisateur_Commandes] FOREIGN KEY([id_utilisateur])
REFERENCES [dbo].[Adresse] ([id_adresse])
GO
ALTER TABLE [dbo].[Utilisateur] CHECK CONSTRAINT [FK_Utilisateur_Commandes]
GO
ALTER TABLE [dbo].[Utilisateur]  WITH CHECK ADD  CONSTRAINT [FK_Utilisateur_Genre] FOREIGN KEY([id_utilisateur])
REFERENCES [dbo].[Genre] ([id_genre])
GO
ALTER TABLE [dbo].[Utilisateur] CHECK CONSTRAINT [FK_Utilisateur_Genre]
GO
ALTER TABLE [dbo].[Utilisateur]  WITH CHECK ADD  CONSTRAINT [FK_Utilisateur_Profil] FOREIGN KEY([id_utilisateur])
REFERENCES [dbo].[Profil] ([id_profil])
GO
ALTER TABLE [dbo].[Utilisateur] CHECK CONSTRAINT [FK_Utilisateur_Profil]
GO
USE [master]
GO
ALTER DATABASE [FoodTruck] SET  READ_WRITE 
GO
