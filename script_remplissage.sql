-----id_famille_repas :
--1-Viennoiserie
--2-boisson
--3-v�g�
--4-viandart
--5-Dessert


----------------------------------------------------------------------------------
---------------------------------------- P'tit Dej -------------------------------
----------------------------------------------------------------------------------

delete from Produits where id_produit>0;
truncate table Type_repas;
delete from Famille_repas where activite=1;

---------Croissant-----------

INSERT [dbo].[Produits]
           ([id_produit]
           ,[libelle_commercial]
           ,[libelle_technique]
           ,[description]
           ,[stock]
           ,[prix_unitaire]
           ,[id_famille_repas])
VALUES 
(1, N'Croissant', N'Croissant', 'Delicieux croissant au beurre de Mamie Jo', 10, 1, 1)

---------Chocolatine-----------

INSERT [dbo].[Produits]
           ([id_produit]
           ,[libelle_commercial]
           ,[libelle_technique]
           ,[description]
           ,[stock]
           ,[prix_unitaire]
           ,[id_famille_repas])
VALUES 
(2, 'Chocolatine', N'Chocolatine', 'Delicieuse Chocolatine au beurre de Mamie Jo', 10, 1, 1)

---------Pain au chocolat-----------
INSERT [dbo].[Produits]
           ([id_produit]
           ,[libelle_commercial]
           ,[libelle_technique]
           ,[description]
           ,[stock]
           ,[prix_unitaire]
           ,[id_famille_repas])
VALUES 
(3, 'Pain au chocolat', N'pain_choco', 'Une viennoiserie venant du Nord au chocolat', 10, 2, 1)

----------------------------------------------------------------------------------
---------------------------------------- Boisson -------------------------------
----------------------------------------------------------------------------------

---------Th�-----------

INSERT [dbo].[Produits]
           ([id_produit]
           ,[libelle_commercial]
           ,[libelle_technique]
           ,[description]
           ,[stock]
           ,[prix_unitaire]
           ,[id_famille_repas])
VALUES 
(4, 'Th� glac�', 'Th�Glac�', 'Un fantastique Th� glac� venant des contreforts de l''Himalaya', 10, 3, 2)

---------Chocolat-----------

INSERT [dbo].[Produits]
           ([id_produit]
           ,[libelle_commercial]
           ,[libelle_technique]
           ,[description]
           ,[stock]
           ,[prix_unitaire]
           ,[id_famille_repas])
VALUES 
(5, 'Chocolat chaud', 'ChocoChaud', 'Un fantastique chocolat chaud au Cacao', 10, 3, 2)

---------Caf�-----------

INSERT [dbo].[Produits]
           ([id_produit]
           ,[libelle_commercial]
           ,[libelle_technique]
           ,[description]
           ,[stock]
           ,[prix_unitaire]
           ,[id_famille_repas])
VALUES 
(6, 'Caf�', 'Caf�', 'Un fantastique chocolat Caf� torr�fi� avec amour', 10, 3, 2)

----------------------------------------------------------------------------------
---------------------------------------- V�g� -------------------------------
----------------------------------------------------------------------------------

---------Fallafel-----------

INSERT [dbo].[Produits]
           ([id_produit]
           ,[libelle_commercial]
           ,[libelle_technique]
           ,[description]
           ,[stock]
           ,[prix_unitaire]
           ,[id_famille_repas])
VALUES 
(7, 'Fallafel', 'Fallafel', 'D�licieuses Fallafels roul�es sous les aisselles', 10, 5, 3)

---------Salade-----------

INSERT [dbo].[Produits]
           ([id_produit]
           ,[libelle_commercial]
           ,[libelle_technique]
           ,[description]
           ,[stock]
           ,[prix_unitaire]
           ,[id_famille_repas])
VALUES 
(8, 'Salade Quinoa', 'Quinoa', 'Une d�licieuse salade de quinoa pour r�veiller le bobo qui sommeille en vous', 10, 5, 3)

---------Burger v�g�-----------

INSERT [dbo].[Produits]
           ([id_produit]
           ,[libelle_commercial]
           ,[libelle_technique]
           ,[description]
           ,[stock]
           ,[prix_unitaire]
           ,[id_famille_repas])
VALUES 
(9, 'Burger v�g�', 'BurgerV�g�', 'Un burger v�g�tarien avec un steak de lentilles !', 10, 8, 3)

---------Pad thai-----------

INSERT [dbo].[Produits]
           ([id_produit]
           ,[libelle_commercial]
           ,[libelle_technique]
           ,[description]
           ,[stock]
           ,[prix_unitaire]
           ,[id_famille_repas])
VALUES 
(10, 'Pad Thai', 'Padthai', 'Un delicieux Pad thai au Soja', 10, 8, 3)

----------------------------------------------------------------------------------
---------------------------------------- Viandart -------------------------------
----------------------------------------------------------------------------------

---------Burger boeuf-----------

INSERT [dbo].[Produits]
           ([id_produit]
           ,[libelle_commercial]
           ,[libelle_technique]
           ,[description]
           ,[stock]
           ,[prix_unitaire]
           ,[id_famille_repas])
VALUES 
(11, 'Burger boeuf', 'BurgerViande', 'Un burger qui � le gout de la viande', 10, 8, 4)

---------Burger Magret-----------

INSERT [dbo].[Produits]
           ([id_produit]
           ,[libelle_commercial]
           ,[libelle_technique]
           ,[description]
           ,[stock]
           ,[prix_unitaire]
           ,[id_famille_repas])
VALUES 
(12, 'Burger Magret', 'BurgerViande', 'Un delicieux burger au magret de canard', 10, 8, 4)

----------------------------------------------------------------------------------
---------------------------------------- Dessert -------------------------------
----------------------------------------------------------------------------------

---------Mousse-----------

INSERT [dbo].[Produits]
           ([id_produit]
           ,[libelle_commercial]
           ,[libelle_technique]
           ,[description]
           ,[stock]
           ,[prix_unitaire]
           ,[id_famille_repas])
VALUES 
(13, 'Mousse au chocolat', 'MousseChoco', 'Une fantastique mousse au chocolat', 10, 3, 5)

---------Tarte citron-----------

INSERT [dbo].[Produits]
           ([id_produit]
           ,[libelle_commercial]
           ,[libelle_technique]
           ,[description]
           ,[stock]
           ,[prix_unitaire]
           ,[id_famille_repas])
VALUES 
(13, 'Tarte citron', 'TarteCitron', 'Une fantastique tarte au citron meringu� !', 10, 3, 5)


-------------------------------------------------------------------------------------------------------------------
------------------------------------------- Famille_repas ----------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------

INSERT [dbo].Famille_repas
           ([id_famille_repas]
           ,[libelle]
		   ,[activite])
		   VALUES 
(1, 'Viennois',1)

INSERT [dbo].Famille_repas
           ([id_famille_repas]
           ,[libelle]
		   ,[activite])
		   VALUES 
(2, 'Boisson',1)

INSERT [dbo].Famille_repas
           ([id_famille_repas]
           ,[libelle]
		   ,[activite])
		   VALUES 
(3, 'V�g�',1)

INSERT [dbo].Famille_repas
           ([id_famille_repas]
           ,[libelle]
		   ,[activite])
		   VALUES 
(4, 'Viandart',1)

INSERT [dbo].Famille_repas
           ([id_famille_repas]
           ,[libelle]
		   ,[activite])
		   VALUES 
(5, 'Dessert',1)

-------------------------------------------------------------------------------------------------------------------
------------------------------------------- Repas ----------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------

-----id_repas :
--1-dej
--2-gouter
--3-diner

INSERT [dbo].Repas
           ([id_repas]
           ,[type_repas]
		   ,[heure_limite_commande]
		   ,[activite])
		   VALUES 
(1, 'Dejeuner', '11:00:00',1)

INSERT [dbo].Repas
           ([id_repas]
           ,[type_repas]
		   ,[heure_limite_commande]
		   ,[activite])
		   VALUES 
(2, 'gouter', '18:00:00',1)

INSERT [dbo].Repas
           ([id_repas]
           ,[type_repas]
		   ,[heure_limite_commande]
		   ,[activite])
		   VALUES 
(3, 'Diner', '23:00:00',1)

-------------------------------------------------------------------------------------------------------------------
------------------------------------------- Type_Repas ----------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------

--dej//viennoiserie
INSERT [dbo].Type_repas
           ([id_repas]
           ,[id_famille_repas])
		   VALUES 
(1,1)

--dej//boisson
INSERT [dbo].Type_repas
           ([id_repas]
           ,[id_famille_repas])
		   VALUES 
(1,2)

--gouter//boisson
INSERT [dbo].Type_repas
           ([id_repas]
           ,[id_famille_repas])
		   VALUES 
(2,2)

--Diner//boisson
INSERT [dbo].Type_repas
           ([id_repas]
           ,[id_famille_repas])
		   VALUES 
(3,2)

--Diner//v�g�
INSERT [dbo].Type_repas
           ([id_repas]
           ,[id_famille_repas])
		   VALUES 
(3,3)

--Diner//viandart
INSERT [dbo].Type_repas
           ([id_repas]
           ,[id_famille_repas])
		   VALUES 
(3,4)

--Diner//Dessert
INSERT [dbo].Type_repas
           ([id_repas]
           ,[id_famille_repas])
		   VALUES 
(3,5)

--gouter//Dessert
INSERT [dbo].Type_repas
           ([id_repas]
           ,[id_famille_repas])
		   VALUES 
(2,5)


-------------------------------------------------------------------------------------------------------------------
------------------------------------------- Actualit� ----------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------

INSERT [dbo].Actualit�s
           ([id_actualite]
           ,[titre]
		   ,[description]
		   ,[image])
		   VALUES 
(1,'Information COVID-19', 'Nous sommes r�ouvert !', 'Picture_1')

INSERT [dbo].Actualit�s
           ([id_actualite]
           ,[titre]
		   ,[description]
		   ,[image])
		   VALUES 
(2,'Nouvel emplacement', 'Nous nous installons devant SII !', 'Picture_2')